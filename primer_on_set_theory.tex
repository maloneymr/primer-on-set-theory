\documentclass{article}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{pxfonts}
\usepackage{exercise}
\usepackage{verbatim}
\usepackage[utf8]{inputenc}

\newcommand{\inl}[0]{\textnormal{\scshape Left }} 
\newcommand{\inr}[0]{\textnormal{\scshape Right }} 

\newtheorem*{thm}{Theorem}

\begin{document}



\centerline{\sc \LARGE A Primer on Set Theory}
\vspace{.5pc}

\section*{Set Basics}
A set is a collection of ``things''. Sets can contain numbers, functions, points in space, letters, colors, or even other sets. 
A set is defined by what it contains. Order doesn't matter, so $\{1, 2\}$ is the same as $\{2, 1\}$. Multiplicity doesn't matter, so $\{1, 1, 2\}$ is the same as $\{1, 2\}$. 

The contents of a set are called its elements (or its members or its points). In symbols, we write $x \in S$ to denote that x is an element of the set S. If x is {\it not} an element of S, we write $x \notin S$ instead.

Many sets are so common that they have standard names:  $\mathbb{N}$ for the natural numbers, $\mathbb{Z}$ for the integers, $\mathbb{Q}$ for the rationals\footnote{the Q is for Quotient}, $\mathbb{R}$ for the reals, $\mathbb{C}$ for the complex numbers. There is also the empty set (or the null set), written either $\{\}$ or $\varnothing$.

If $A$ contains everything $B$ contains, we say $A \subseteq B$. If $B$ contains everything $A$ contains, we write $A \supseteq B$. If the two sets contain {\it exactly} the same elements as one another, we say they are equal, and write $A = B$. (Note this is the same as both $A \subseteq B$ and $A \supseteq B$). A subset $A$ of $B$ is called a {\it proper} subset if $A \subseteq B$, but $A \neq B$. If $A$ is a proper subset of $B$, we denote\footnote{CAUTION! Some authors use $A \subset B$ to mean that $A$ is any subset, not just a proper subset. Other authors avoid confusion by writing $A \subsetneq B$ for proper subsets.} this $A \subset B$.

To talk about subsets, we sometimes use a notation called set builder notation. Here is an example: $\{x \in \mathbb{Z} \mid \textnormal{$x$ is even}\}$. This defines a subset of the integers, and in this case, it defines the even integers. Another example would be $\{(x, y) \in \mathbb{R}^2 | y = x^2\}$, which happens to be a parabola-shaped curve in the plane. 

\section*{Operations on Sets}
There are many basic operations on sets. The union of $A$ and $B$, denoted $A \cup B$, is a set which contains all the elements of $A$ together with all the elements of $B$. (Although remember that a set can't "contain two copies" of an element). The intersection of $A$ and $B$, written $A \cap B$, is the set of elements that are in both $A$ and $B$. 

To give an example of union and intersection, let E be the set of even integers and O be the set of odd integers. We can see that $E \cup O = \mathbb{Z}$, since every integer is either in E or O, and that $E \cap O = \varnothing$, since no integer is both even and odd. If the intersection of two sets is the empty set, we call them disjoint sets.

Another operation called set subtraction, denoted $A \setminus B$, is defined as everything in $A$ that is not also in $B$. So $\{1,2,3,4,5\} \setminus \{1,3,5,7\} = \{2,4\}$. Sometimes, we are working with a bunch of subsets of a particular set $U$ for a long time in a chapter. The complement of a set $A$ (implicitly taken relative to a "universal" set $U$) is defined as $U \setminus A$, and is denoted $\overline{A}$.

Again, some examples. The set $\mathbb{R} - \mathbb{Q}$ is the set of irrational numbers. Given a universal set $\mathbb{Z}$, we note that $\overline{\{x \in \mathbb{Z} \mid \textnormal{$x$ is even}\}} = \{x \in \mathbb{Z} \mid \textnormal{$x$ is odd}\}$. In English, we say the complement of the even integers are the odd integers.

Two more operations worth talking about are the cartesian product and the disjoint union. The cartesian product of $A$ and $B$, written $A \times B$, is the set of (ordered) pairs $(a, b)$ with $a \in A$ and $b \in B$. The cartesian product is famous for being used for analytic geometry, and it was a major advancement in the mathematics of the 1600s. (By the way, $\mathbb{R}^2 = \mathbb{R} \times \mathbb{R}$). 

The disjoint union, written $A \amalg B$, is the set of "tagged" elements in $A$ or $B$. If $a \in A$, then $\inl a \in A \amalg B$, and if $b \in B$, then $\inr b \in A \amalg B$. This is similar to the union, but it "forces" the two sets to be disjoint if they aren't already. For example, $\{1,2,3\} \amalg \{3, 4, 5, 6\}$ is the set:
$\{\inl 1$, $\inl 2$, $\inl 3$, $\inr 3$, $\inr 4$, $\inr 5$, $\inr 6 \}$. Being created from a set of 3 elements and a set of 4 elements, it contains a total of 7 elements\footnote{Note that because the elements are tagged, $\inl 3 \neq \inr 3$}.

The power set $\wp(A)$ of a set $A$ is defined as the set of all subsets of $A$. For example, $\wp(\{1, 2\}) = \{\varnothing, \{1\}, \{2\}, \{1, 2\}\}$. The powerset has the handy property that, if $A \subseteq B$, then $A \in \wp(B)$. 

\section*{Functions}

Functions are perhaps the most important mathematical objects in all of modern mathematics. A function is a rule that assigns inputs to unique outputs. One familiar example is the function which takes an integer and squares it, sometimes denoted $x \mapsto x^2$. Often, we define a function and give it a name using the notation $f(x) = x^2$. This defines $f$ as a function. The notation $f(x)$ is the output of the function $f$ given an input $x$.

Every function is associated with two sets. A function's domain is the set of all possible inputs. A function's codomain is the set of all potential outputs. We declare that our function $x \mapsto x^2$ has a domain of $\mathbb{Z}$ and a codomain of $\mathbb{Z}$ -- that is, the inputs and outputs of this functions are integers. When a function $f$ has domain $A$ and codomain $B$, we write $f \colon A \rightarrow B$.

A function need not be able to output every element of its codomain. While $f(x) = x^2$ has a codomain of $\mathbb{Z}$, it's clear that there is no $x \in \mathbb{Z}$ such that $f(x) = -1$, for instance. The set of values the function is actually able to output is called its image (or range).

For any set, $A$, there is a special function called the identity function on $A$, noted $id_{A} \colon A \rightarrow A$ defined by $id_{A}(x) = x$. Given any two functions\footnote{Note that in order to compose two functions, the codomain of the first function must equal the domain of the second function.} $f \colon A \rightarrow B$ and $g \colon B \rightarrow C$, we can compose them: $g \circ f \colon A \rightarrow C$ is defined by $(g \circ f)(x) = g(f(x))$\footnote{Be careful about the order of functions when you compose them!}. It is useful to note three algebraic properties\footnote{Identities and composition together with these three laws are how we define the notion of a category, which plays a very important role in modern mathematics.} of function composition:
\nopagebreak
\[ f \circ id_{A} = f \]
\[ id_{B} \circ f = f \]
\[ h \circ (g \circ f) = (h \circ g) \circ f \]


\section*{Surjections, Injections, and Bijections}

If a function's image is equal to its codomain, then the function is called surjective (or onto\footnote{Sûr means onto in French, thus surjection}). The function the function $f \colon \mathbb{Z} \rightarrow \mathbb{Z}$ defined as $f(x) = x^2$ is not surjective. On the other hand, the function $g \colon \mathbb{Z} \rightarrow \mathbb{Z}$ defined $g(x) = x+1$ is surjective. 

If a function never maps the two distinct inputs to the same output, we call that function injective (or one-to-one). Again, we see $f(x) = x^2$ is not injective, because it maps both $-2$ and $2$ to $4$. But $h\colon \mathbb{Z} \rightarrow \mathbb{Z}$ defined by $h(x) = -x$ is injective.

Surjections and injections are closely related. Let $f \colon A \rightarrow B$ be a function. Given an element $y \in B$, we are going to think about the set of "solutions" (in $x$) to the equation $f(x) = y$. If for every $y$, the equation $f(x) = y$ has {\it at least} one solution, then $f$ is surjective. If for every $y$, this equation has {\it at most} one solution, then $f$ is surjective.

In the very special case where $f$ is both surjective and injective, we call $f$ a bijection (or a one-to-one correspondence or an isomorphism). For a bijection $f$, we know, for every $y$, there is exactly one $x$ such that $f(x) = y$. This allows us to define an inverse function, $f^{-1}(y) = x$. If $f \colon A \rightarrow B$, then $f^{-1} \colon B \rightarrow A$. If we compose $f$ with its inverse (in either order) we get the identity function on $A$ or $B$:

\[f^{-1} \circ f = id_{A}\]
\[f \circ f^{-1} = id_{B}\]

The identity function on $A$ is trivially a bijection. The function $f \colon \mathbb{Z} \rightarrow \mathbb{Z}$ defined $f(x) = x+1$ is also a bijection. It's inverse is $f^{-1}(x) = x-1$. A more interesting example is the exponential function from calculus $\exp \colon \mathbb{R} \rightarrow \mathbb{R}^+$. The inverse for the exponential is the natural logarithm, $\ln \colon \mathbb{R}^+ \rightarrow \mathbb{R}$. Here we see that there is a bijection between two different sets. 

If we are able to find a bijection between two sets $A$ and $B$, we say the two sets are isomorphic, and we write $A \cong B$. We saw just above that $\mathbb{Z} \cong \mathbb{Z}$ and $\mathbb{R} \cong \mathbb{R}^+$. I am going to assert\footnote{If $A$ and $B$ are both finite sets, the set of functions $A \rightarrow B$ is also finite. Given enough patience, pens, and paper, you can write them all out by hand. If you want to, try proving by brute force that there are no bijections between these two sets.} that there is no bijection between the sets $\{1, 2\}$ and $\{1, 2, 3\}$. Thus, $\{1, 2\} \ncong \{1,2,3\}$.

\section*{Cardinality}

The cardinality of a set is a measure of how large it is. For finite sets, the cardinality is just the number of elements. For infinite sets, however, we cannot rely on simply counting (since we would never get to the end). But we can compare any two sets, infinite or not, by looking at the bijections between them.

Two set are said to have the same cardinality if they are isomorphic. A set $B$ is said to have a greater cardinality than a set $A$ (written $A \prec B$) if $A \ncong B$, but there exists a proper subset $B^\prime \subset B$ such that $A \cong B'$. For example, the set $\{1, 2, 3\} \prec \mathbb{Z}$.

One of the first interesting results in set theory was the discovery that some infinite sets are "bigger" than others. More precisely, it's possible that $A \prec B$, even when both $A$ and $B$ are infinite. 

\begin{thm}{Cantor's Theorem.}
\normalfont
For all sets $A$, we have $A \prec \wp(A)$.
\end{thm}

\begin{proof}
To prove this, we need to first show that $A \cong B$ for some subset $B \subset \wp(A)$. Then, we need to show $A \ncong \wp(A)$.

The first part is straightforward. We can consider the set of singletons, $S = \{\{x\} \mid x \in A\}$\footnote{For instance, if $A = \{1,2,3\}$, then $S = \{\{1\}, \{2\}, \{3\}\}$}. We define a function $i \colon A \rightarrow S$ as $i(x) = \{x\}$. We can verify this is a bijection, and thus, $A \cong S$.

Now for the second half. We need to prove $A \ncong \wp(A)$. We will do this by assuming a bijection $f \colon A \rightarrow \wp(A)$ and then deriving a contradiction. 

Define a subset $D = \{x \in A \mid x \notin f(x)\}$. Since $f$ is a bijection, it is also a surjection, and so there must be some $x \in A$ such that $f(x) = D$. We then ask the question, $x \in D$?

If the answer to this question is yes, then $x \in D$, meaning $x \notin f(x)$. But since $f(x) = D$, $x \notin D$. So this case leads to a contradiction.

If the answer to the question above is no, and $x \notin S$, then $x$ must\footnote{Otherwise, $x$ would have been in $D$} be in $f(x)$. But again, $f(x) = D$, so $x \in D$. So this case also leads to a contradiction.

Since every case leads to a contradiction, we must refute our original assumption.
\end{proof}

So if some infinite sets have greater cardinality than others, we can begin to ask, ``which infinite sets have which cardinalities?'' It turns out a good number of infinite sets we use have the same cardinality as $\mathbb{N}$. We call these countably infinite sets. The countably infinite sets include $\mathbb{Z}$, $\mathbb{Q}$, the even integers, the set of prime numbers, and the set of all made-up words you can spell with the English alphabet. 

Another important (but distinct) cardinality is the cardinality of $\mathbb{R}$. This cardinality is also shared by $\mathbb{C}$, the set of functions $\mathbb{N} \rightarrow \mathbb{N}$, and all intervals of the real line (no matter how small in length).




\end{document}



